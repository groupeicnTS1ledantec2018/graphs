def mkEmptyGraph(size):
    network=[]
    for i in range(size):
        network.append([])
    return network

def mkCompleteGraph(size):
    network=[]
    for i in range(size):
        network.append([])
        for j in range(size):
            if j!=i:
                network[i].append(j)
    return network

def mkPerfectSquareSumGraph(size,power=2):
    network=[]
    for i in range(size):
        network.append([])
        j=2
        while pow(j,power)-i<size:
            if pow(j,power)-i>=0 and pow(j,power)!=2*i:
                network[i].append(pow(j,power)-i)
            j+=1
    return network

def mkPolygonalGraph(size,power=2,multigraph=0,pseudograph=0):
    network=[]
    for i in range(size):
        network.append([])
        for j in range(power):
            a=i-j
            while a<0:
                a+=size
            if (multigraph or (a not in network[i])) and (pseudograph or (a!=i)) :
                network[i].append(a)
            b=i+j
            while b>=size:
                b-=size
            if (multigraph or (b not in network[i])) and (pseudograph or (b!=i)) :
                network[i].append(b)
    return network

def mkPolyhedralGraph(size,power=2,multigraph=0,pseudograph=0):
    network=[]
    return network