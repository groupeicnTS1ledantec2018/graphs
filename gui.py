# Python 3
import tkinter
from tkinter import *
from tkinter.messagebox import *

class GUI(object):
        
    def __init__(self):
        root = self.root = tkinter.Tk()
        root.title('Programme Calculs sur Graphes')
        root.geometry('900x600')
                            
        def on_closing():
            if askokcancel("Quit", "Do you want to quit?"):
              root.destroy()

    # make the top right close button minimize (iconify) the main window
        root.protocol("WM_DELETE_WINDOW", on_closing)

    # make Esc exit the program
        root.bind('<Escape>', lambda e: on_closing())

    # create a menu bar with an Exit command
        menubar = tkinter.Menu(root)
        filemenu = tkinter.Menu(menubar, tearoff=0)        
        filemenu.add_command(label="Ouvrir")
        filemenu.add_command(label="Nouveau")
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=on_closing)
        menubar.add_cascade(label="File", menu=filemenu)
        root.config(menu=menubar)

    
    # create a text
        fichier = open("bonjour.txt", "r")
        tout = fichier.read()
        message = Label(root, text=tout , width=100, bg="ivory").pack(side=TOP, padx=5, pady=5)
        
        
        Bouton1 = Button(root, text="Graphe 1", width=25, height=15, bg="ivory").pack(side=LEFT, padx=20, pady=20)
        Bouton2 = Button(root, text="Graphe 2", width=25, height=15, bg="ivory").pack(side=LEFT, padx=20, pady=20)
        Bouton3 = Button(root, text="Graphe 3", width=25, height=15, bg="ivory").pack(side=LEFT, padx=20, pady=20)
        
        #Frame1 = Frame(Bouton1, Bouton2, Bouton3, borderwidth=2, relief=GROOVE)
        #Frame1.pack(side=LEFT, padx=30, pady=30)

# frame 2
        #Frame2 = Frame(fenetre, borderwidth=2, relief=GROOVE)
        #Frame2.pack(side=LEFT, padx=10, pady=10)

# frame 3 dans frame 2
        #Frame3 = Frame(Frame2, bg="white", borderwidth=2, relief=GROOVE)
        #Frame3.pack(side=RIGHT, padx=5, pady=5)

# Ajout de labels
        #Label(Frame1, text="Frame 1").pack(padx=10, pady=10)
        #Label(Frame2, text="Frame 2").pack(padx=10, pady=10)
        #Label(Frame3, text="Frame 3",bg="white").pack(padx=10, pady=10)
        
        #Scrollbar(Bouton1, activebackground="red")
        
gui = GUI()
gui.root.mainloop()
