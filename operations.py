import string
from random import *
import os

def randString(minLen=16,maxLen=32,charset=string.ascii_letters+string.digits):
    if(minLen>=maxLen):
        maxLen=minLen+1
    return "".join(choice(charset) for x in range(randint(minLen, maxLen)))

def permutations(iterable, r=None):
    pool = tuple(iterable)
    n = len(pool)
    r = n if r is None else r
    if r > n:
        return
    indices = list(range(n))
    cycles = list(range(n, n-r, -1))
    yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                yield tuple(pool[i] for i in indices[:r])
                break
        else:
            return

def permutations2(iterable, fixed=0):
    for i in range(fixed,len(iterable)):
        #print("Start : {} (Depth : {})".format(iterable,fixed))
        iterable[fixed:] = [iterable[i]] + iterable[fixed:i] + iterable[i+1:]
        #print("Mutation : {} (Depth : {}) ".format(iterable,fixed))
        if fixed==len(iterable)-1:
            yield iterable
        for e in permutations2(iterable,fixed+1):
            yield e
        iterable[fixed:] = iterable[fixed+1:i+1] + [iterable[fixed]] + iterable[i+1:]
        #print("Backtracking : {} (Depth : {}) ".format(iterable,fixed))

def permutations3(iterable):
    iterable = tuple(iterable)
    if len(iterable) <= 1:
        yield iterable
        return
    for i, x in enumerate(iterable):
        for sub_perm in permutations3(iterable[:i] + iterable[i+1:]):
            yield (x,) +  sub_perm
