from makeGraphs import *
from findPaths import *
from graph import *

t=int(input("Type of Network (0 to 4) : "))
n=int(input("Size of Network (strictly positive integer) : "))

if t==0:
    graph=Graph(mkEmptyGraph(n))
elif t==1:
    graph=Graph(mkCompleteGraph(n))
elif t==2:
    p=int(input("Power of Network (strictly positive integer) : "))
    graph=Graph(mkPerfectSquareSumGraph(n,p))
elif t==3:
    p=int(input("Power of Network (strictly positive integer) : "))
    q1=int(input("Multigraph (0 or 1) : "))
    q2=int(input("Pseudograph (0 or 1) : "))
    graph=Graph(mkPolygonalGraph(n,p,q1,q2))
elif t==4:
    p=int(input("Power of Network (strictly positive integer) : "))
    q1=int(input("Multigraph (0 or 1) : "))
    q2=int(input("Pseudograph (0 or 1) : "))
    graph=Graph(mkPolyhedralGraph(n,p,q1,q2))
else:
   print("Unknown Type...")

print(graph)