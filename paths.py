from operations import *

#Brute Force Search - Time-Complexity : O(n!)
def hamiltonianPaths1(graph, cycle=False, first=None, last=None):
	name = "cycle" if cycle else "path"
	rules = ""
	if first!=None:
		rules += " starting at {}".format(graph.verticesName[first])
	if first!=None and last!=None:
		rules += " and"
	if last!=None:
		rules += " ending at {}".format(graph.verticesName[last])
	n=0
	paths=[]
	for permutation in permutations(graph.vertices):
		for i in range(len(permutation)-1+int(cycle)):
			if [permutation[i],permutation[(i+1)%len(permutation)]] not in graph.edges:
				#print("No edge from {} to {}!".format(graph.verticesName[permutation[i]], graph.verticesName[permutation[(i+1)%len(permutation)]]))
				break
			#print("Edge from {} to {}.".format(graph.verticesName[permutation[i]], graph.verticesName[permutation[(i+1)%len(permutation)]]))
		else:
			if (first==None or first==permutation[0]) and (last==None or last==permutation[-1]):
				path=""
				for i in permutation:
					path+=graph.verticesName[i]+", "
				else:
					path=path[:-2]
				print("{} is a hamiltonian {}{}!".format(path,name,rules))
				n+=1
				paths.append(list(permutation))
	
	print("{} hamiltonian {}(s){} found!".format(n,name,rules))
	return paths

#Optimization : if first and/or last vertex specified, then, we do not test the permutations not starting/ending with the vertex
def hamiltonianPaths1bis(graph, cycle=False, first=None, last=None):
	name = "cycle" if cycle else "path"
	rules = ""
	verts=list(graph.vertices)
	if first!=None:
		rules += " starting at {}".format(graph.verticesName[first])
		verts.remove(first)
	if first!=None and last!=None:
		rules += " and"
	if last!=None:
		rules += " ending at {}".format(graph.verticesName[last])
		verts.remove(last)
	n=0
	paths=[]
	for permutation in permutations(verts):
		permutation=list(permutation)
		if first!=None:
			permutation[:0]=[first]
		if last!=None:
			permutation[len(permutation):]=[last]
		for i in range(len(permutation)-1+int(cycle)):
			if [permutation[i],permutation[(i+1)%len(permutation)]] not in graph.edges:
				#print("No edge from {} to {}!".format(graph.verticesName[permutation[i]], graph.verticesName[permutation[(i+1)%len(permutation)]]))
				break
			#print("Edge from {} to {}.".format(graph.verticesName[permutation[i]], graph.verticesName[permutation[(i+1)%len(permutation)]]))
		else:
			path=""
			for i in permutation:
				path+=graph.verticesName[i]+", "
			else:
				path=path[:-2]
			print("{} is a hamiltonian {}{}!".format(path,name,rules))
			n+=1
			paths.append(list(permutation))
	
	print("{} hamiltonian {}(s){} found!".format(n,name,rules))
	return paths

def eulerianPaths1(graph, cycle=False, first=None, last=None):
    return []

def allPaths1(graph, cycle=False, first=None, last=None):
    return []

def shortestPaths1(graph,first,last):
    return []
