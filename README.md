Projet réalisé par : Thomas GHOBRIL et Corentin BOUNON.

Le code peut être réutilisé, modifié, et distribué librement.

Language : Python.
Librairies : Tkinter, matplotlib 2.1, NetworkX 2.1, NumPy 1.14, SciPy library 1.0, SymPy 1.1, SciKit-Learn 0.19, TensorFlow 1.5.