class Graph:
    def __init__(self,network):
        self.network=network
        
        self.vertices=[]
        self.verticesName=[]
        self.verticesWeight=[]
        self.verticesDegree=[]
        self.edges=[]
        self.edgesName=[]
        self.edgesWeight=[]
        
        for i in range(len(network)):
            self.vertices.append(i)
            self.verticesName.append("Point {}".format(str(i)))
            self.verticesWeight.append(1)
            deg=len(network[i])
            for j in network[i]:
                if i==j:
                    deg+=1
            self.verticesDegree.append(deg)
        
        for i in range(len(network)):
            for j in network[i]:
                    self.edges.append([i,j])
                    self.edgesName.append("From {} to {}".format(self.verticesName[i],self.verticesName[j]))
                    self.edgesWeight.append(1)
    
    def eulerize(self, semi=False):
        return

class Tree:
    nodes = []
    
    def __init__(self, name, root):
        self.name = name
        self.nodes.append(Node(root,"/"))
    
    def addNode(self,name,path):
        self.nodes.append(Node(name,path))
    
class Node:
    def __init__(self,name,path):
        self.name = name
        self.path = path
        