#####

for i in range(12):
	x=time.time()
	graph=Graph(mkPerfectSquareSumGraph(i,2))
	print("Network : {} \n".format(graph.network))
	ham = hamiltonianPaths1(graph)
	#print(ham)
	print("Time for graph of size {} : {} seconds.\n".format(i,time.time()-x))

#####

graph=Graph([[1,2,4],[0,2,3,4],[0,1,3],[1,2,4],[0,1,3]])
graph.verticesName=['Paris','Nantes','Brest','Marseille','Strasbourg']
ham = hamiltonianPaths1bis(graph,True,3,2)
#print(ham)

#####